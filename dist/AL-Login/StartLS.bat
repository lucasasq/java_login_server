@echo off
title Aion 2.7 Login Server Console
color 3
:start
echo Aion 2.7  Login Server.

SET PATH="C:\Program Files\Java\JavaJDK_6\bin"

echo.
REM -------------------------------------
REM Default parameters for a basic server.
java -Xms16m -Xmx64m -server -cp ./libs/*;AL-Login.jar com.aionlightning.loginserver.LoginServer
REM
REM -------------------------------------

SET CLASSPATH=%OLDCLASSPATH%


if ERRORLEVEL 1 goto error
goto end
:error
echo.
echo Login Server Terminated Abnormaly, Please Verify Your Files.
echo.
:end
echo.
echo Login Server Terminated.
echo.
pause
